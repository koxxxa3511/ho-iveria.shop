<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MainSliderSaveRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'type_id' => 'required|min:1|numeric'
        ];
    }

    public function authorize(): bool
    {
        return Auth::check();
    }
}
