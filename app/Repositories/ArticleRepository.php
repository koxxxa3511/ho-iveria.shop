<?php

namespace App\Repositories;

use App\Models\Article;
use App\Models\Article as Model;
use App\Models\Role;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ArticleRepository extends BaseRepository
{
    public function __construct() {
        parent::__construct(new Model());
    }

    /**
     * Get the model for edit in the admin panel
     *
     * @param $id
     * @return mixed
     */

    public function getEdit($id)
    {
        $result = $this->startConditions()->find($id);
        return $result;
    }

    /**
     * Get the model for front end side
     *
     * @param $id
     * @return mixed
     */

    public function getById($id, $relations = ['translate'])
    {
        return parent::getById($id, $relations);
    }

    /**
     * Get list for output by paginator
     *
     * @param null $perPage
     * @param string[] $relations
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */

    public function getAllWithPaginate(int $perPage, string $orderBy, array $relations = ['rubric.translate', 'translate'], array $select = [])
    {
        $result = parent::getAllWithPaginate($perPage, $orderBy, $relations, $select);
        return $result;
    }

    /**
     * Get all articles by rubric id
     *
     * @param $rubric_id
     * @return mixed
     */

    public function getAllByRubricId($rubric_id)
    {
        $result = $this->startConditions()
                       ->whereRubricId($rubric_id)
                       ->with('translate')
                       ->get();

        return $result;
    }

}
