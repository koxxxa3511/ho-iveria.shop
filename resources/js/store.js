let store = {
    state: {
        products: [],
        cart: {},
        cartShowed: false,
        loading: true,
        cart_count: 0,
        favorites: [],
        auth: window.AUTH || false,
        product: window.PRODUCT
    },
    actions: {
        async getData({state}) {
            await axios.get('/data/get').then(({data}) => {
                state.cart = data.cart;
                state.products = data.products;
                state.favorites = data.favorites;
                state.loading = false;
            });

        },
        addToCart({state}, payload) {
            let link = '/cart/add/';
            if (typeof payload == 'number') {
                link += payload;
            } else {
                link += payload['item_id'] + '?count=' + payload['count'];
            }
            axios.get(link).then((response) => {
                state.cart = response.data;
            })

        },
        removeFromCart({state}, payload) {
            axios.get('/cart/remove/' + payload).then(({data}) => {
                state.cart = data;
            });

        },
        changeCount({state}, payload) {
            let count = payload['count'],
                product_id = payload['product_id'];

            state.cart[product_id]['count'] = count;
            axios.get('/cart/product/' + product_id + '/set-count/' + count)
                .then((response) => {
                    state.cart = response.data;
                });

        },
        addToFavorite({state}, payload) {
            if (_.includes(state.favorites, payload)) {
                state.favorites.splice(state.favorites.indexOf(payload), 1);
            } else {
                state.favorites.push(payload);
            }
            axios.get('/favorite/add/' + payload);
        },
        clearCart({state}) {
            state.cart = {
                amount: 0
            };
            axios.get('/cart/clear');

        }
    }
}

export default store;
