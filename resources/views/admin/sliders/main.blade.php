@extends('layouts.admin')
<?php
/**
 * @mixin \App\Models\MainSlider
 */
?>
@section('content')
    <h2>
        @if($mainSlider->exists())
            @lang("Слайдер: #$mainSlider->id")
        @else
            @lang("Cлайдер: Главный")
        @endif
    </h2>
    <form action="{{ route('admin-main-slider.save') }}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{ $mainSlider->id }}">
        <div class="row">
            <div class="col-lg">
                <label for="type_id">@lang("Отображение")</label>
                <select name="type_id" id="type_id" class="form-control" required>
                    <option
                        value="{{ $mainSlider::TYPE_DESKTOP }}" {{ attr_selected($mainSlider->type_id == $mainSlider::TYPE_DESKTOP) }}>@lang("admin.main_slider_type_1")</option>
                    <option
                        value="{{ $mainSlider::TYPE_MOBILE }}" {{ attr_selected($mainSlider->type_id == $mainSlider::TYPE_MOBILE) }}>@lang("admin.main_slider_type_2")</option>
                </select>
            </div>
            <div class="col-lg">
                <label for="type_id">@lang("Картинка")</label>
                <input type="file" name="file" id="file" class="form-control" accept="image/jpeg image/png"
                    {{ $mainSlider->exists() ? '' : 'required'}}>
                @if($mainSlider->path)
                    <img src="{{ $mainSlider->full_path }}" alt="" class="img-fluid mt-2">
                @endif
            </div>
        </div>
        <div class="text-end mt-3">
            <button type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-save"></i>
                @lang("Сохранить")
            </button>
        </div>
    </form>
    @if(count($images))
        <div class="table-responsive mt-3">
            <table class="table table-striped table-sm table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Оторбражение</th>
                    <th>Картинка</th>
                    <th>*</th>
                </tr>
                </thead>
                <tbody>
                @foreach($images as $image)
                    <tr>
                        <td>{{ $image->id }}</td>
                        <td>
                            @lang("admin.main_slider_type_$image->type_id")
                        </td>
                        <td>
                            <a href="{{ $image->full_path }}" data-fancybox>
                                <img src="{{ $image->full_path }}" alt="" class="img-fluid" width="50">
                            </a>
                        </td>
                        <td>

                            <a href="{{ route('admin-main-slider.edit', ['mainSlider' => $image]) }}"
                               class="link-dark">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="{{ route('admin-main-slider.delete', ['mainSlider' => $image]) }}"
                               class="link-danger" data-confirm="Уверенны?">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @elseif(!$mainSlider->exists())
        <div class="row">
            <div class="col-12">
                <div class="alert alert-warning mt-3" role="alert">
                    Картинок нет...
                </div>
            </div>
        </div>
    @endif
@endsection
