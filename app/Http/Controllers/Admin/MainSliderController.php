<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\MainSliderSaveRequest;
use App\Models\MainSlider;
use App\Repositories\MainSliderRepository;
use App\Services\ImageUploadService;

class MainSliderController extends Controller
{
    private $mainSliderRepository;

    /**
     * Service to save image
     *
     * @var ImageUploadService|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $imageUploadService;

    public function __construct() {
        $this->mainSliderRepository = app(MainSliderRepository::class);
        $this->imageUploadService = app(ImageUploadService::class);
    }

    public function index()
    {
        $images = $this->mainSliderRepository->getAll();
        $mainSlider = new MainSlider();
        return view('admin.sliders.main', compact('images', 'mainSlider'));
    }

    public function edit(MainSlider $mainSlider)
    {
        $images = [];
        return view('admin.sliders.main', compact('images', 'mainSlider'));
    }

    public function save(MainSliderSaveRequest $request)
    {
        $mainSlider = MainSlider::updateOrCreate([
                                                     'id' => $request->input('id')
                                                 ],
                                                 [
                                                     'type_id' => $request->input('type_id')
                                                 ]);

        if ($request->hasFile('file')) {
            $mainSlider->path = $request->file('file')->store('main-slider');

            $this->imageUploadService->uploadInWebP($mainSlider, $request->file('file'), ['file_path' => 'main-slider']);
        }

        $mainSlider->save();

        flash('Сохранено');
        return redirect()->route('admin-main-slider.index');
    }

    public function delete(MainSlider $mainSlider)
    {
        MainSlider::destroy($mainSlider->id);

        flash('Удалено');
        return to_route('admin-main-slider.index');
    }
}
