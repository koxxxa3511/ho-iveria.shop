<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Closure;

class IsRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
    	$user = $request->user();
    	if(!$user || $user->role_id == Role::ID_USER){
    		abort(403);
	    }
        return $next($request);
    }
}
