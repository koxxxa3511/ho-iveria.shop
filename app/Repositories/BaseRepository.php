<?php

namespace App\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository extends CoreRepository
{

    protected $model;

    public function __construct(Model $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    protected function getModelClass()
    {
        return $this->model;
    }

    /**
     * Get all records for the front end side
     *
     * @param $id
     * @return mixed
     */

    public function getAll(array $relations = [])
    {
        return $this->startConditions()
                    ->with($relations)
                    ->get();
    }

    /**
     * Get record by ID
     *
     * @param int $id
     */

    public function getById(int $id, array $relations = [])
    {
        return $this->startConditions()
                    ->with($relations)
                    ->findOrFail($id);
    }

    /**
     * Find records by ID
     *
     * @param array $ids
     */

    public function find(array $ids, array $relations = [])
    {
        return $this->startConditions()
                    ->with($relations)
                    ->find($ids);
    }

    /**
     * Get list for output by paginator
     *
     * @param $perPage
     * @param array $relations
     * @param $orderBy
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */

    public function getAllWithPaginate(int $perPage, string $orderBy, array $relations = [], array $select = [])
    {
        $result = $this->startConditions()
                       ->with($relations);

        if (!empty($select)) {
            $result->select($select);
        }
        return $result->latest($orderBy)
                      ->paginate($perPage);
    }
}
