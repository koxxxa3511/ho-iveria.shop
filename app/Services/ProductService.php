<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductService extends BaseService
{
    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    public function handle(Request $request)
    {
        $baseInputs = [
            'id' => $request->input('id')
        ];
        $changeInputs = [
            'product_category_id' => $request->input('product_category_id'),
            'price'               => $request->input('price')
        ];
        return parent::updateOrCreate($baseInputs, $changeInputs);
    }
}
