<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ArticleSaveRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'rubric_id' => 'required|numeric|exists:rubrics,id',
            'title' => 'required|max:255|string'
        ];
    }

//    public function authorize(): bool
//    {
//        return Auth::check();
//    }
}
