<?php

namespace App\Repositories;

use App\Models\ProductCategory as Model;

class ProductCategoryRepository extends BaseRepository {


    public function __construct() {
        parent::__construct(new Model());
    }

    /**
     * Get all product categories
     *
     * @param string[] $relations
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */

    public function getAll($relations = ['translate']){
        return parent::getAll($relations)->sortByDesc('id');
    }

    /**
     * Get the model for edit in the admin panel
     *
     * @param $id
     * @return mixed
     */

    public function getEdit($id, $relations = ['translate']){
        return $this->startConditions()->with($relations)->find($id);
    }

    /**
     * Get the model for the front-end side
     *
     * @param $id
     * @return mixed
     */

    public function getById($id, $relations = ['translate']){
        return parent::getById($id, $relations);
    }


    /**
     * Get a list of rubrics to show select list.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */

    public function getForSelect($relations = ['translate']){
        $columns = ['id', 'image_path'];

        $result =  $this->startConditions()->with($relations)
            ->select($columns)
            ->get();

        return $result;
    }

}
