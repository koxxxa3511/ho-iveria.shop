<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['logout' => false]);
Route::get('/logout',[App\Http\Controllers\Auth\LoginController::class,'logout'])->name('logout');
Route::get('/set-locale/{locale}',[PublicController::class,'setLocale'])->name('set-locale');

Route::group(['middleware' => 'HtmlMinifier'], function() {
    Route::get('/', [App\Http\Controllers\PublicController::class, 'index'])
         ->name('index');

    Route::get('/category/{category}/product/{slug}', [PublicController::class, 'product'])
         ->name('public-product');
    Route::get('/moi-tovari', [PublicController::class, 'favorite'])
         ->name('favorite');
    Route::view('/dostavka-i-oplata', 'public.deliver')
         ->name('public.delivery');
    Route::view('/kontakti', 'public.contacts')
         ->name('public.contacts');

    Route::get('/data/get', [PublicController::class, 'getData'])
         ->name('public.get-data');
    Route::get('/favorite/add/{product}', [PublicController::class, 'addToFavorite'])
         ->name('public.add-to-favorite');

    Route::get('cart/add/{product}', [PublicController::class, 'addToCart'])
         ->name('public.add-to-cart');
    Route::get('cart/product/{product}/set-count/{count}', [PublicController::class, 'setCount'])
         ->name('public.cart-set-count');
    Route::get('cart/remove/{product}', [PublicController::class, 'removeFromCart'])
         ->name('public.remove-from-cart');
    Route::get('cart/clear', [PublicController::class, 'cartClear'])
         ->name('public.cart-clear');

    Route::get('rubrics', [BlogController::class, 'index'])
         ->name('public.rubric-index');
    Route::get('rubric/{slug}', [BlogController::class, 'rubric'])
         ->name('public.rubric');
    Route::get('rubric/{rubricSlug}/article/{articleSlug}', [BlogController::class, 'article'])
         ->name('public-article');

    Route::get('/api/login', [\App\Http\Controllers\Auth\LoginController::class, 'apiLogin']);

    Route::prefix('profile')
         ->group(function () {
             Route::get('/', [ProfileController::class, 'index'])
                  ->name('profile');
             Route::post('/save', [ProfileController::class, 'save'])
                  ->name('profile-save');
         });
});

//examples
Route::view('/example/modal','example.modal');

