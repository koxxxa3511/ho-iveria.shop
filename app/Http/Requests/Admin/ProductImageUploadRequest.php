<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProductImageUploadRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'product_id' => 'required|numeric|exists:products,id',
            'file'       => 'required|image'
        ];
    }

    public function authorize(): bool
    {
        return \Auth::check();
    }
}
