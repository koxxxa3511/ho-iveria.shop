<?php

namespace App\Repositories;

use App\Models\MainSlider as Model;

class MainSliderRepository extends BaseRepository
{
    public function __construct() {
        parent::__construct(new Model());
    }

    /**
     * @return string
     */

    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Get the model for edit in the admin panel
     *
     * @param $id
     * @return mixed
     */

    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    /**
     * Get list for output by paginator
     *
     * @param null $perPage
     * @param string[] $relations
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */

    public function getAllWithPaginate($perPage, $orderBy, array $relations = [], $select = [])
    {
        $result = parent::getAllWithPaginate($perPage, $orderBy, $relations, $select);
        return $result;
    }

}
