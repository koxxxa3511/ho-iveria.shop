<?php

namespace App\Services;

use App\Models\Article;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ArticleService extends BaseService
{
    public function __construct(Article $article) {
        $this->model = $article;
    }

    public function handle(Request $request){
        $baseInputs = [
            'id' => $request->input('id')
        ];
        $changeInputs = [
            'rubric_id' => $request->input('rubric_id')
        ];
        return parent::updateOrCreate($baseInputs, $changeInputs, $request->file('file'));
    }
}
