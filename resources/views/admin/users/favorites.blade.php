@extends('layouts.admin')
@section('content')
    <h2>{{ $user->name }}: Избранные товары</h2>
    <div class="table-responsive mt-3">
        <table class="table-sm table-hover table-striped table">
            <thead>
            <tr class="text-center">
                <th>ID товара</th>
                <th>Картинка товара</th>
                <th>Название товара</th>
                <th>Добавлен</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr class="text-center">
                    <td>{{ $product->id }}</td>
                    <td>
                        <a href="{{ $product->main_image->full_path }}" data-fancybox>
                            <img src="{{ $product->main_image->full_path }}" alt="" class="img-fluid rounded" width="30">
                        </a>
                    </td>
                    <td>{{ $product->translate->title }}</td>
                    <td>{{ $product->pivot->created_at->format('d.m.Y H:i') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
