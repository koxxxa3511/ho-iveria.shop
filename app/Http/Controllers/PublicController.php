<?php

namespace App\Http\Controllers;

use App\Models\FavoriteUser;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Repositories\CartRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\TranslateRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller
{
    /**
     * @var ProductRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $productRepository;
    /**
     * @var TranslateRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $translateRepository;
    /**
     * @var ProductCategoryRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $productCategoryRepository;
    /**
     * @var CartRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $cartRepository;

    public function __construct()
    {
        $this->productRepository = app(ProductRepository::class);
        $this->translateRepository = app(TranslateRepository::class);
        $this->productCategoryRepository = app(ProductCategoryRepository::class);
        $this->cartRepository = app(CartRepository::class);
    }

    /**
     * Homepage
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */

    public function index()
    {
        $products = $this->productRepository->getAllWithTranslates(['main_image', 'translate', 'category.translate']);

        $novelties = $products->sortByDesc('id')->slice(0, 7);
        $tops = $products->where('is_top', true);
        $sales = $products->where('sale_price', '!=', null);

        return view('index', compact('novelties', 'tops', 'sales', 'products'));
    }

    /**
     * Method to change language on site
     *
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function setLocale($locale)
    {
        $locales = config("app.locales");

        if (!in_array($locale, $locales)) {
            $locale = config("app.locale");
        }
        Session::put("locale", $locale);
        return redirect('/');
//        return back();
    }

    /**
     * Product page
     *
     * @param $category
     * @param $slug
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */

    public function product($category, $slug)
    {
        $translate = $this->translateRepository->getBySlugName($slug, Product::class);
        $translate_c = $this->translateRepository->getBySlugName($category, ProductCategory::class);

        if (empty($translate) || empty($translate_c)) {
            abort(404);
        }

        $product = $this->productRepository->getById($translate->translateable_id, []);
        $category = $this->productCategoryRepository->getById($translate_c->translateable_id, []);

        return view('public.product', compact('product', 'translate', 'category'));
    }

    /**
     * Get all data, for the basket and favorites page
     *
     * @return array
     */

    public function getData()
    {
        $favorites = Session::get('favorite_ids', []);
        $cart = $this->getCart();
        $products = $this->productRepository->getAllWithTranslates(['main_image', 'translate', 'category.translate']);

        return compact('cart', 'favorites', 'products');
    }

    /**
     * Favorites page
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */

    public function favorite()
    {
        $products = $this->productRepository->find(Session::get('favorite_ids', []));

        return view('profile.favorite', compact('products'));
    }

    /**
     * Add product to favorite
     *
     * @param Product $product
     * @return bool[]
     */


    public function addToFavorite(Product $product)
    {
        $favorite_ids = Session::get('favorite_ids', []);
        $user = Auth::user();
        if (!in_array($product->id, $favorite_ids)) {
            $favorite_ids[] = $product->id;
            Session::put('favorite_ids', $favorite_ids);
            if ($user) {
                 FavoriteUser::firstOrCreate([
                    'user_id' => $user->id,
                    'product_id' => $product->id
                ]);
            }
        } else {
            $favorite_ids = Session::pull('favorite_ids', []);
            $new_ids = [];
            foreach ($favorite_ids as $favorite_id) {
                if ($favorite_id != $product->id) {
                    $new_ids[] = $favorite_id;
                }
            }
            Session::put('favorite_ids', $new_ids);
            if ($user) {
                FavoriteUser::where(['user_id' => $user->id, 'product_id' => $product->id])->delete();
            }
        }
        return ['success' => true];
    }

    /**
     * Add product to the basket
     *
     * @param Request $request
     * @param Product $product
     * @return array
     */

    public function addToCart(Request $request, Product $product)
    {
        $cart = \Cart::session(session()->getId());
        $item = $cart->get($product->id);
        if (!$item) {
            $cart->add([
                    'id' => $product->id,
                    'name' => $product->translate->title,
                    'price' => $product->price,
                    'quantity' => $request->input('count') ?? 1,
                    'attributes' => [],
                    'associatedModel' => Product::class
                ]);
        } else {
            $cart
                ->update($product->id, [
                    'quantity' => $request->input('count') ?? 1
                ]);
        }

        return $this->getCart();
    }

    /**
     * Get data of the basket
     *
     * @return array
     */

    public function getCart(): array
    {
        return $this->cartRepository->getAllInArray();
    }

    /**
     * Remove from the basket
     *
     * @param Product $product
     * @return array
     */

    public function removeFromCart(Product $product)
    {
        \Cart::session(session()->getId())->remove($product->id);

        return $this->getCart();
    }

    /**
     * Change count of product in the basket
     *
     * @param $product_id
     * @param $count
     * @return array
     */

    public function setCount($product_id, $count)
    {
        $cart  =\Cart::session(session()->getId());
        $item = $cart->get($product_id)->toArray();
        if ($item) {
            $item['quantity'] = $count * 1;

            $cart->remove($product_id);
            $cart->add($item);
        }

        return $this->getCart();
    }

    /**
     * Remove all products from the basket
     *
     * @return bool[]
     */

    public function cartClear()
    {
        \Cart::session(session()->getId())->clear();

        return ['success' => true];
    }
}
