<?php

namespace App\Observers;

use App\Models\Translate;

class TranslateObserver
{
    public function creating(Translate $translate)
    {
        $translate->locale = getLocale();
        $translate->content = translateUploadImage($translate, 'articles');
    }

    public function updating(Translate $translate)
    {
        /**
         * * Find in the content image in base64 format, save image on server and change path
         */
        $translate->content = translateUploadImage($translate, 'articles');
    }
}
