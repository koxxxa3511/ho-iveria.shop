<?php

namespace App\Observers;

use App\Models\ProductImage;

class ProductImageObserver
{

    public function deleting(ProductImage $productImage)
    {
        \Storage::delete($productImage->path);
    }
}
