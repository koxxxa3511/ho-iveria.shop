<?php

namespace App\Observers;

use App\Models\Product;
use App\Models\Translate;

class ProductObserver
{

    public function deleting(Product $product)
    {
        \Storage::deleteDirectory('/products/product-' . $product->id);
        $product->images()->delete();
    }
}
