<?php

namespace App\Models;

use App\Traits\Models\MainTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, MainTrait;
    protected $fillable = ['id', 'product_category_id', 'price'];
    protected $appends = [
        'transTitle'
    ];

    public function translate(){
        return $this->morphOne(Translate::class, 'translateable')->whereLocale(getLocale());
    }

    public function total_translates(){
        return $this->morphMany(Translate::class, 'translateable');
    }

    public function main_image(){
        return $this->hasOne(ProductImage::class)->oldestOfMany('id');
    }

    public function getMainImagePathAttribute(){
        return $this->main_image->full_path;
    }

    public function images(){
        return $this->hasMany(ProductImage::class)->oldest('id');
    }
    public function category(){
        return $this->belongsTo(ProductCategory::class, 'product_category_id', 'id');
    }

    public function getCategorySlugAttribute(){
        return $this->category->translate->slug ?? '';
    }

    public function getTransSlugAttribute(){
        return $this->translate->slug ?? '';
    }

    public function getTransTitleAttribute(){
        return $this->translate->title ?? '';
    }
}
