<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

abstract class BaseService
{
    /**
     * Initial new model
     *
     * @var Model
     */
    public $model;

    /**
     * Service to save image
     *
     * @var ImageUploadService|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $imageUploadService;

    public function __construct() {
        $this->imageUploadService = app(ImageUploadService::class);
    }

    /**
     * Update or create new record in DB, using:
     * baseInputs array - data for searching,
     * ChangedInputs array - data what will be changed,
     * Image - If exists - will be save, using model's method
     *
     * @param array $baseInputs
     * @param array $changeInputs
     * @param false $image
     * @return mixed
     */

    protected function updateOrCreate(array $baseInputs, array $changeInputs, $image = false)
    {
        $result = $this->model::updateOrCreate($baseInputs, $changeInputs);

        if ($image) {
            $path = $this->parsePath(get_class($this->model) . '/' . $result->id);
            $result->setImage($image->store($path));

            // Need to review...
            $this->imageUploadService->uploadInWebP($result, $image, ['file_path' => $path]);
        }

        return $result;
    }

    /**
     * Find first or create new record in DB, using:
     * baseInputs array - data for searching or creating
     * Image - if exists will be save, using model\s method
     *
     * @param array $baseInputs
     * @param false $image
     * @return mixed
     */

    public function firstOrCreate(array $baseInputs, $image = false){
        $result = $this->model::firstOrCreate($baseInputs);

        if($image){
            $path = $this->parsePath(get_class($this->model) . '/' . $result->id);
            $result->setImage($image->store($path));
        }

        return $result;
    }

    /**
     * Parse string, replacing "\" to "/"
     *
     * @param $path"
     * @return string"
     */

    private function parsePath($path){
        return Str::replace('\\', '/', $path);
    }
}
