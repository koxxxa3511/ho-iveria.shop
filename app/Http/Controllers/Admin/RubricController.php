<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RubricRequest;
use App\Models\Rubric;
use App\Models\Translate;
use App\Repositories\RubricRepository;
use App\Services\RubricService;
use App\Services\TranslateService;

class RubricController extends Controller
{
    /**
     * Using to get data from rubrics table
     *
     * @var RubricRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $repository;

    /**
     * Using to save data in rubrics table
     *
     * @var RubricService|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $service;

    /**
     * Using to save data in translates table
     *
     * @var TranslateService|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $translateService;

    public function __construct()
    {
        $this->repository = app(RubricRepository::class);
        $this->service = app(RubricService::class);
        $this->translateService = app(TranslateService::class);
    }

    public function index()
    {
        $rubrics = $this->repository->getAll();

        return view('admin.rubrics.index', compact('rubrics'));
    }

    public function create()
    {
        $rubric = new Rubric();

        return view('admin.rubrics.form', compact('rubric'));
    }

    public function edit($id)
    {
        if($id == Rubric::WITHOUT_RUBRIC){
            return back()->withErrors('Denied');
        }
        $rubric = $this->repository->getForEdit($id);

        return view('admin.rubrics.form', compact('rubric'));
    }

    public function save(RubricRequest $request)
    {
        if($request->input('id') == Rubric::WITHOUT_RUBRIC){
            return back()->withErrors('Denied');
        }
        $rubric = $this->service->handle($request);

        $this->translateService->handle($rubric, $request);

        return to_route('admin-rubric.index');
    }

    public function delete($id){
        if($id == Rubric::WITHOUT_RUBRIC){
            return back()->withErrors('Denied');
        }
        Rubric::destroy($id);

        flash('Удалено');
        return redirect()->route('admin-rubric.index');
    }
}
