@extends('layouts.admin')
@section('content')
    <h2>
        @if($article->exists)
            Редактирование статьи: #{{ $article->id }} {{ $article->translate->title ?? '' }}
        @else
            Дорбавление новой статьи
        @endif
    </h2>
    <form action="{{ route('admin-article.save') }}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{ $article->id }}">
        <div class="row mb-3">
            <div class="col-lg">
                <label for="title">Заголовок</label>
                <input type="text" name="title" id="title" class="form-control" value="{{ $article->translate->title ?? '' }}" required>
            </div>
            <div class="col-lg">
                <label for="rubric_id">Рубрика</label>
                <select name="rubric_id" id="rubric_id" class="form-control" required>
                    <option value="">-Выберите рубрику-</option>
                    @foreach($rubrics as $rubric)
                        <option value="{{ $rubric->id }}" {{ attr_selected($rubric->id == $article->rubric_id) }}>{{ $rubric->translate->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg">
                <label for="file">Картинка</label>
                <input type="file" name="file" id="file" accept="image/jpeg, image/png" {{ !$article->exists ? 'required' : '' }}>
            </div>
        </div>
        <textarea name="content" cols="30" rows="10" class="summernote">{!! $article->translate->content ?? '' !!}</textarea>
        <div class="text-end mt-3">
            <button type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-save"></i>
                Сохранить
            </button>
        </div>
    </form>
@endsection

@push('scripts')
    <!-- include summernote css/js -->
    <link href="/assets/summernote/summernote-lite.min.css" rel="stylesheet">
    <script src="/assets/summernote/summernote-lite.min.js"></script>
    <script>
        $(function () {
            $('.summernote').summernote({
                height: 300,
                // toolbar: [
                //     ['style', ['style']],
                //     ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                //     ['fontname', ['fontname']],
                //     ['fontsize', ['fontsize']],
                //     ['color', ['color']],
                //     ['para', ['ol', 'ul', 'paragraph', 'height']],
                //     ['table', ['table']],
                //     ['insert', ['link']],
                //     ['view', ['undo', 'redo', 'fullscreen', 'codeview', 'help']]
                // ]
            });
        })
    </script>
@endpush
