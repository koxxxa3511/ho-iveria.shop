<?php

namespace App\Observers;

use App\Models\Article;
use App\Models\Rubric;

class RubricObserver
{

    public function updating(Rubric $rubric)
    {
        if ($rubric->isDirty('image_path') && !empty($rubric->image_path)) {
            \Storage::delete($rubric->getOriginal('image_path'));
        }
    }

    public function deleting(Rubric $rubric)
    {
        \Storage::deleteDirectory("rubrics/{$rubric->id}");
        Article::whereRubricId($rubric->id)
               ->update([
                            'rubric_id' => Rubric::WITHOUT_RUBRIC
                        ]);
    }
}
