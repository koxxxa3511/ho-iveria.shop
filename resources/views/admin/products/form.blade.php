@extends('layouts.admin')
@section('content')
    <h2>
        @if($product->exists)
            Редактирование {{ $product->translate->title ?? '' }}
        @else
            Добавление товара
        @endif
    </h2>
    <form action="{{ route('admin-product.save') }}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $product->id }}">
        <div class="mb-3 row">
            <div class="col-lg">
                <label for="title">Название</label>
                <input type="text" name="title" id="title" class="form-control" required
                       value="{{ $product->translate->title ?? '' }}">
            </div>
            <div class="col-lg">
                <label for="product_category">Категория</label>
                <select name="product_category_id" id="product_category_id" class="form-control" required>
                    <option value="">-Категория-</option>
                    @foreach($product_categories as $product_category)
                        <option
                            value="{{ $product_category->id }}" {{ attr_selected($product->product_category_id == $product_category->id) }}>{{ $product_category->translate->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg">
                <label for="price">Цена</label>
                <input type="number" name="price" id="price" class="form-control" required step=".01" min="0"
                       value="{{ $product->price }}">
            </div>
        </div>
        <div class="mb-3">
            <label for="summernote">Описание</label>
            <textarea name="content" id="summernote" cols="10"
                      rows="10">{!! $product->translate->content ?? '' !!}</textarea>
        </div>
        <div class="text-end mt-3">
            <button type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-save"></i>
                Сохранить
            </button>
        </div>
    </form>
@endsection

@push('scripts')
    <!-- include summernote css/js -->
    <link href="/assets/summernote/summernote-lite.min.css" rel="stylesheet">
    <script src="/assets/summernote/summernote-lite.min.js"></script>
    <script>
        $(function () {
            $('#summernote').summernote({
                height: 300,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ol', 'ul', 'paragraph', 'height']],
                    ['table', ['table']],
                    ['insert', ['link']],
                    ['view', ['undo', 'redo', 'fullscreen', 'codeview', 'help']]
                ]
            });
        })
    </script>
@endpush
