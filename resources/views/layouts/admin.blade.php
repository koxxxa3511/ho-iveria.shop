<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(Auth::user()->role_id != \App\Models\Role::ID_USER)
        <link rel="stylesheet" href="{{ asset('/css/admin.css') }}">
    @endif
    <link rel="stylesheet" href="{{ mix('/css/public.css') }}">

    <title>{{ config('app.name') }}</title>

    <style>
        table tr td, table tr th{
            font-size: 14px;
        }
    </style>

    @stack('head')
</head>
<body>
<div>
    @include('layouts.menu')
    <div class="content" id="{{ Route::current()->getName() }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <x-admin.menu></x-admin.menu>
                </div>
                <div class="col-lg-9">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <x-footer></x-footer>
</div>

<div id="ajax-modal-container"></div>
<script src="{{ mix('/js/public.js') }}"></script>
<script src="{{ mix('/js/vue.js') }}"></script>
@stack('scripts')

@include('includes.flash')
</body>
</html>
