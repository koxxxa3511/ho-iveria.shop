<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CategorySaveRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'titles' => 'required|array|size:2',
            'file' => 'required_if:id,null|image'
        ];
    }

    public function authorize(): bool
    {
        return Auth::check();
    }

    public function messages()
    {
        return [
            'file.required_if'=> 'Для новой записи картинка обязательна к заполнению'
        ];
    }
}
