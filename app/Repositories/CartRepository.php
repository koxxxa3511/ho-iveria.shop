<?php

namespace App\Repositories;

use App\Models\Product;

class CartRepository {

    /**
     * Get all items in cart and parse into array for front end
     * @return array
     */

    public function getAllInArray(){
        $productRepository = new ProductRepository();
        $cart = [];
        $cart['amount'] = 0;
        foreach (\Cart::session(session()->getId())->getContent() as $item) {

            $product = $productRepository->getById($item->id, ['main_image', 'translate']);

            if(empty($product)){
                abort(404);
            }
            $cart[$product->id]['id'] = $product->id;
            $cart[$product->id]['image'] = $product->main_image->full_path;
            $cart[$product->id]['title'] = $product->translate->title;
            $cart[$product->id]['slug'] = $product->translate->slug;
            $cart[$product->id]['price'] = $product->price;
            $cart[$product->id]['count'] = $item->quantity;
            $cart[$product->id]['category'] = $product->category->translate->slug;
            $cart['amount'] += $item->getPriceSum();
        }
        return $cart;
    }
}
