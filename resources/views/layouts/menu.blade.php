<header id="header"  v-cloak>
    <div class="cart-bg" v-if="$store.state.cartShowed" @click="$store.state.cartShowed = !$store.state.cartShowed"></div>
    <div class="d-none d-lg-block">
        <div class="top">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <a href="/" class="logo">
                            <img src="/images/logo.png" alt="{{ env('APP_NAME', "Logo") }}" class="img-fluid" loading="lazy">
                        </a>
                    </div>
                    <div class="col-lg">
                        <ul class="top-links">
                            <li class="top-link-item">
                                <a href="{{ route('public.delivery') }}" class="top-link">@lang("Доставка и оплата")</a>
                            </li>
                            <li class="top-link-item">
                                <a href="#" class="top-link">@lang("Отзывы")</a>
                            </li>
                            <li class="top-link-item">
                                <a href="{{ route('public.rubric-index') }}" class="top-link">@lang("Блог")</a>
                            </li>
                            <li class="top-link-item">
                                <a href="{{ route('public.contacts') }}" class="top-link">@lang("Контакты")</a>
                            </li>
                            <li class="top-link-item">
                                @if(getLocale() == 'ru')
                                    <a href="{{ route('set-locale', 'ua') }}" class="top-link">@lang("UA")</a>
                                @elseif(getLocale() == 'ua')
                                    <a href="{{ route('set-locale', 'ru') }}" class="top-link">@lang("RU")</a>
                                @endif
                            </li>
                        </ul>
                        <ul class="top-phones">
                            <li class="top-phone">
                                <a href="#">
                                    <i class="fa fa-phone"></i>
                                    {{ phone_formatting(settings('phone-1'))}}
                                </a>
                            </li>
                            <li class="top-phone">
                                <a href="#">
                                    <i class="fa fa-user"></i>
                                    {{ phone_formatting(settings('phone-2')) }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="under-top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6" id="search">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <i class="fa fa-search"></i>
                            </div>
                            <input type="text" name="search" id="search-field" class="form-control" required
                                   placeholder="@lang("Быстрый поиск товаров")" v-model="search">
                            <span class="search-clear text-danger" @click="search = ''"
                                  v-if="search != ''">
                                <i class="fa fa-times"></i>
                            </span>
                        </div>
                        <div class="result" :class="{'block-showed' : search != ''}">
                            <table class="table table-sm">
                                <tbody>
                                <tr v-for="product in sortedProducts">
                                    <td width="50" class="text-center p-1" valign="middle">
                                        <a :href="'/category/'+product.category.translate.slug+'/product/'+product.translate.slug"
                                           class="p-0">
                                            <img :src="product.main_image.full_path" alt="" class="img-fluid" loading="lazy">
                                        </a>
                                    </td>
                                    <td class="text-start title">
                                        <a :href="'/category/'+product.category.translate.slug+'/product/'+product.translate.slug">
                                            @{{ product.translate.title }}</a>
                                    </td>
                                    <td class="price text-center">
                                        <span> @{{ product.price }}</span>
                                        грн
                                    </td>
                                    <td class="cart" @click="addToCart(product.id)">
                                        <i class="fa fa-shopping-cart"></i>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="col-lg text-right position-relative">
                        <ul class="links">
                            <li class="link-item">
                                <a href="{{ route('favorite') }}" class="link-favorite">
                                    <i class="fa fa-heart"></i>
                                </a>
                            </li>
                            <li class="link-item">
                                @guest
                                    <a href="{{ route('login') }}" class="link-auth ajax-modal">
                                        <i class="fa fa-user"></i>
                                    </a>
                                @else
                                    <a href="{{ route('profile') }}" class="link-auth">
                                        <i class="fa fa-user"></i>
                                    </a>
                                @endguest
                            </li>
                            <li class="link-item">
                                <a href="#" class="link-cart" @click.prevent="$store.state.cartShowed = !$store.state.cartShowed">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span v-if="cart_count > 0" class="cart-count">
                                        @{{ cart_count }}
                                    </span>
                                </a>
                                <div class="cart" :class="{'block-showed' : $store.state.cartShowed}">
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th class="text-start"
                                                style="padding-left: 21px;">@lang("Название товара")</th>
                                            <th class="text-center">@lang("Кол-во")</th>
                                            <th class="text-center">@lang("Сумма")</th>
                                            <th class="total-trash"><i class="fa fa-trash" @click="clearCart"></i></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="(product, product_id) in $store.state.cart" v-if="product_id != 'amount'">
                                            <td width="50" class="text-center p-1" valign="middle">
                                                <a :href="'/category/'+product.category+'/product/'+product.slug"
                                                   class="p-0">
                                                    <img :src="product.image" alt="" class="img-fluid" loading="lazy">
                                                </a>
                                            </td>
                                            <td class="text-start title">
                                                <a :href="'/category/'+product.category+'/product/'+product.slug">
                                                    @{{ product.title }}</a>
                                                <p><span class="total-count">@{{ product.count }} @lang("шт")</span></p>
                                            </td>
                                            <td class="text-center count">
                                                <input type="number" data-mask="number"
                                                       class="cart-count" min="1" :value="product.count"
                                                       @input="changeCount(product_id, $event.target.value)">
                                            </td>
                                            <td class="text-center">
                                                <p><span>@{{ product.price * product.count }}</span> грн</p>
                                                <div class="little-count mt-1" v-if="product.count > 1">
                                                    @{{ product.count }} x @{{ product.price }} грн
                                                </div>
                                            </td>
                                            <td>
                                                <div @click.prevent="removeItem(product_id)" class="trash">
                                                    <i class="fa fa-trash"></i>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr class="total">
                                                <td colspan="3" class="text-end">
                                                    @lang("Всего к оплате")
                                                </td>
                                                <td class="text-center"><span>@{{ $store.state.cart['amount'] }}</span> грн</td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-lg-none">
        <nav class="under-top-menu">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="nav-brand">
                            <a href="/" class="logo">
                                <img src="/images/logo.png" alt="{{ env('APP_NAME', "Logo") }}" class="img-fluid" loading="lazy">
                            </a>
                        </div>
                    </div>
                    <div class="col text-end">
                        <button class="btn bars" type="button" data-bs-toggle="offcanvas" data-bs-target="#mobile-menu"
                                aria-controls="offcanvasExample">
                            <i class="fas fa-bars"></i>
                        </button>
                    </div>
                </div>
            </div>
        </nav>
        <div class="offcanvas offcanvas-start" tabindex="-1" id="mobile-menu"
             aria-labelledby="offcanvasExampleLabel">
            <div class="offcanvas-header">
                <a href="/" class="offcanvas-title">
                    <img src="/images/logo.png" alt="{{ env('APP_NAME', "Logo") }}" class="img-fluid" loading="lazy">
                </a>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                        aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <ul class="mobile-menu">
                    <li>
                        <a href="/">@lang("Главная")</a>
                    </li>
                    <li>
                        <a href="#">@lang("Доставка и оплата")</a>
                    </li>
                    <li>
                        <a href="#">@lang("Отзывы")</a>
                    </li>
                    <li>
                        <a href="#">@lang("Блог")</a>
                    </li>
                    <li>
                        <a href="#">@lang("Контакты")</a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}">@lang("Выход")</a>
                    </li>
                </ul>
                <div class="offcanvas-bottom">
                    <div class="row">
                        <div class="col">
                            <ul class="offcanvas-list">
                                <li>
                                    <a href="#">{{ phone_formatting(settings('phone-1')) }}</a>
                                </li>
                                <li>
                                    <a href="#">{{ phone_formatting(settings('phone-2')) }}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col text-end">
                            <ul class="offcanvas-list">
                                <li>
                                    <a href="{{ route('set-locale', 'ua') }}"
                                       class="{{ getLocale() == 'ua' ? 'active' : '' }}">UA</a>
                                </li>
                                <li>
                                    <a href="{{ route('set-locale', 'ru') }}"
                                       class="{{ getLocale() == 'ru' ? 'active' : '' }}">RU</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
