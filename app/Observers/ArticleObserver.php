<?php

namespace App\Observers;

use App\Models\Article;
use Illuminate\Support\Facades\Storage;

class ArticleObserver
{
    public function updating(Article $article)
    {
        if ($article->isDirty('image_path') && !empty($article->getOriginal('image_path'))) {
            Storage::delete($article->getOriginal('image_path'));
        }
    }

    public function deleting(Article $article)
    {
        \Storage::deleteDirectory("/articles/{$article->id}");
    }
}
