<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProductRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'product_category_id' => 'required|numeric|exists:product_categories,id',
            'price' => 'required|numeric|between:0.1,100000', // from 1 to 100 000
            'title' => 'string|required|min:3|max:255'
        ];
    }

    public function authorize(): bool
    {
        return Auth::check();
    }
}
