<?php

namespace App\Models;

use App\Traits\Models\MainTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rubric extends Model
{
    use HasFactory, MainTrait;

    const WITHOUT_RUBRIC = 1;

    public $timestamps = false;

    protected $fillable = ['id'];

    public function translate()
    {
        return $this->morphOne(Translate::class, 'translateable')
                    ->whereLocale(getLocale())
                    ->withDefault([
                                      'title' => ''
                                  ]);
    }

    public function getImageAttribute()
    {
        return '/storage/' . $this->image_path;
    }


    /**
     * Image saver for Service
     * @param $image
     */

    public function setImage($image){
        $this->image_path = $image;
        $this->save();
    }
}
