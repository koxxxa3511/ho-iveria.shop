<div class="breadcrumbs">
    <div>
        <a href="/">
            <i class="fa fa-home"></i>
        </a>
    </div>
    @yield('breadcrumbs')
</div>
