@extends('layouts.admin')

@section('content')
    <form action="{{ route('admin-settings-save') }}" method="post">
        @csrf

        <div class="row">
            @foreach($settings as $setting)
                <div class="col-md-4 mb-2">
                    <label for="{{ $setting->key }}">@lang("settings.{$setting->key}")</label>
                    <input type="text" class="form-control" name="{{ $setting->key }}" value="{{ $setting->value }}">
                </div>
            @endforeach
        </div>
        <div class="text-center form-group">
            <button class="btn btn-primary px-sm-5">@lang('Save')</button>
        </div>
    </form>
@endsection
