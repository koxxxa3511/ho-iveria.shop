<?php
function flash($msg, $type = 'success'){
	Session::flash( 'flash_msg', __( $msg ) );
	Session::flash( 'flash_type', $type );

    return back();
}

function money($value, $char = '.'){
	return number_format($value, 2, $char, ' ');
}
function settings($key,$default = null){
	static $data;
	if(is_null($data)){
		$data = \App\Models\Setting::all()->pluck('value','key')->toArray();
	}
	return Arr::get($data,$key,$default);
}
function _floor($number,$decimals){
	$mul = 10 ** $decimals;
	$number = floor($number * $mul) / $mul;
	return round($number,$decimals);
}

function route_active($route_name, $active_class = 'active'){
	$name = Route::currentRouteName();
	$route_name = Arr::wrap($route_name);
	return in_array($name,$route_name) ? $active_class : null;
}
function attr_selected($bool){
	return $bool ? 'selected' : null;
}
function attr_required($bool){
	return $bool ? 'required' : null;
}
function attr_checked($bool){
	return $bool ? 'checked' : null;
}
function display_size($size){
	$units = ['B','K','M','G'];
	$unit_idx = 0;
	while($size > 1024 && isset($units[$unit_idx+1])){
		$size /= 1024;
		$unit_idx++;
	}
	return round($size,2).' '.$units[$unit_idx];
}
function getLocale(){
    return app()->getLocale();
}

function phone_formatting($phone){
    if(  preg_match( '/^(\d{3})(\d{3})(\d{2})(\d{2})$/', $phone,  $matches ) )
    {
        $result = "($matches[1])". ' ' . $matches[2] . '-' . $matches[3] . '-' . $matches[4];
        return $result;
    }
    return false;
}

function translateUploadImage($model, $folder){
    $text = $model->content;

    if (preg_match_all('/src=[\'"]data:image\/(png|jpg|jpeg|gif);base64,([a-zA-Z0-9+\/]+={0,2})[\'"]/', $text, $matches)) {
        if (count($matches[0]) > 0 && !$model->exists) {
            $model->save();
        }
        foreach ($matches[0] as $i => $replace) {
            $ext = $matches[1][$i];
            if ($img = @base64_decode($matches[2][$i])) {
                $md5 = md5($img);
                $src = "/uploads/{$folder}/{$model->id}/$md5.$ext";
                $path = public_path($src);

                \File::makeDirectory(dirname($path), 0755, true, true);
                \File::put($path, $img);
                $text = str_replace($replace, "src=\"$src\"", $text);
            } else {
                $text = str_replace($replace, '', $text);
            }
        }
    }
    return $text;
}

/**
 * @param $route
 * @return \Illuminate\Http\RedirectResponse
 */
function to_route($route){
    return redirect()->route($route);
}
