@extends('layouts.admin')
@section('content')
    <h2>
        @if($rubric->exists)
            Редактирование {{ $rubric->translate->title }}
        @else
            Добавление товара
        @endif
    </h2>
    <form action="{{ route('admin-rubric.save') }}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{ $rubric->id }}">
        <div class="mb-3 row">
            <div class="col-lg">
                <label for="title">Название</label>
                <input type="text" name="title" id="title" class="form-control" required
                       value="{{ $rubric->translate->title }}">
            </div>
            <div class="col-lg">
                <label for="type_id">@lang("Картинка")</label>
                <input type="file" name="image" id="file" class="form-control" accept="image/jpeg image/png"
                    {{ $rubric->exists() ? '' : 'required'}}>
                @if($rubric->image_path)
                    <div class="text-center">
                        <img src="{{ $rubric->image }}" alt="" class="img-fluid mt-2 w-50">
                    </div>
                @endif
            </div>
        </div>
        <div class="text-end mt-3">
            <button type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-save"></i>
                Сохранить
            </button>
        </div>
    </form>
@endsection

@push('scripts')
    <!-- include summernote css/js -->
    <link href="/assets/summernote/summernote-lite.min.css" rel="stylesheet">
    <script src="/assets/summernote/summernote-lite.min.js"></script>
    <script>
        $(function () {
            $('#summernote').summernote({
                height: 300,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ol', 'ul', 'paragraph', 'height']],
                    ['table', ['table']],
                    ['insert', ['link']],
                    ['view', ['undo', 'redo', 'fullscreen', 'codeview', 'help']]
                ]
            });
        })
    </script>
@endpush
