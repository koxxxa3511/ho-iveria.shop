<footer>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg">
                <p>©2013-{{ date('Y') }} @lang("Интернет-магазин «Ho Iveria»")</p>
                <p>@lang("Все права прописаны в конституции.")</p>
                <ul class="links">
                    <li>
                        <a href="#">@lang("Договор оферты")</a>
                    </li>
                    <li><a href="#">@lang("Подписка")</a></li>
                </ul>
            </div>
            <div class="col-lg text-center logo">
                <img src="/images/logo.webp" alt="{{ env('APP_NAME', "Logo") }}" class="img-fluid" loading="lazy">
            </div>
            <div class="col-lg text-end">
                <p>@lang("Контактные телефоны::")</p>
                <p>
                    <a href="#">{{ phone_formatting(settings('phone-1')) }}</a>
                </p>
                <p>
                    <a href="#">{{ phone_formatting(settings('phone-2')) }}</a>
                </p>
            </div>
        </div>
    </div>
</footer>
