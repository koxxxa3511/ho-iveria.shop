<?php

namespace App\Services;

use Buglinjo\LaravelWebp\Webp;
use Illuminate\Database\Eloquent\Model;

class ImageUploadService
{
    public function uploadInWebP(Model $model, $file, array $options = [])
    {
        $fileName = str_replace(".{$file->getClientOriginalExtension()}", '', $file->getClientOriginalName());
        $fileName = str_replace('.', '_', $fileName);

        $webp = Webp::make($file);
        $filePath = "{$options['file_path']}/{$fileName}.webp";
        if ($webp->save(storage_path('app/public/' . $filePath))) {
            \Storage::delete($model->path);
            $model->setImage($filePath);
        }
    }
}
