<?php

namespace App\Repositories;

use App\Models\User as Model;

class UserRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(new Model());
    }

    /**
     * Get the model for edit in the admin panel
     *
     * @param $id
     * @return mixed
     */

    public function getEdit($id)
    {
        return $this->startConditions()
                    ->find($id);
    }

    /**
     * Get list for output by paginator
     *
     * @param null $perPage
     * @param string[] $relations
     * @param string $latest
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */

    public function getAllWithPaginate(int $perPage, string $orderBy, array $relations = ['role'], $select = [])
    {
        if (empty($select)) {
            $select = ['id', 'name', 'email', 'role_id', 'created_at'];
        }
        $result = parent::getAllWithPaginate($perPage, $orderBy, $relations, $select);
        return $result;
    }

}
