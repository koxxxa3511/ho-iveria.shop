@extends('layouts.default')
@section("breadcrumbs")
    <div>
        <a href="#">{{ $category->transTitle }}</a>
    </div>
    <div class="active">
        <a href="#">{{ $translate->title }}</a>
    </div>
@endsection
@section('content')
    @include('includes.breadcrumbs')
    <div class="box mt-3{{ $product->sale_price ? ' with-sale' : '' }}" id="product">
        <div class="row mt-3">
            <div class="col-lg">
                <div class="text-center position-relative d-none d-lg-block">
                    @if($product->sale_price)
                        <img src="/images/sale.png" alt="" class="img-fluid sale-image">
                    @endif
                    <img src="{{ $product->mainImagePath }}" alt="" class="img-fluid main-photo">
                </div>
                @include('includes.public.product.gallery')
            </div>
            <div class="col-lg">
                <h1>{{ $translate->title ?? '' }}</h1>
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        @if($product->sale_price)
                            <div class="price">
                                <div class="old-price">
                                    <span>{{ number_format($product->price, 0, '.', ' ') }}</span>
                                    грн
                                </div>
                                <div class="new-price">
                                    <span>{{ number_format($product->sale_price, 0, '.', ' ') }}</span>
                                    грн
                                </div>
                            </div>
                        @else
                            <div class="price">
                                <span>{{ number_format($product->price, 0, '.', ' ') }}</span>
                                грн
                            </div>
                        @endif
                    </div>
                    <div class="col-lg">
                        <div class="d-inline-block">
                            <div class="btn btn-buy" @click="addToCart({{ $product->id }})"
                                 :class="{disabled : count < 1}" :disabled="count < 1">
                                <i class="fa fa-shopping-cart"></i>
                                @lang("Купить")
                            </div>
                            <div class="counter">
                                <label>@lang("Количество"):</label>
                                <div>
                                    <i class="minus" @click="subItem">-</i>
                                    <input type="number" data-mask="number" min="1" value="1" autocomplete="off"
                                           id="count" v-model="count">
                                    <i class="plus" @click="count++">+</i>
                                </div>
                            </div>
                        </div>
                        <div class="add-to-favorite" @click="addToFavorite({{ $product->id }})">
                            <i class="fa fa-heart" v-if="_.includes(favorites, {{ $product->id }})"></i>
                            <i class="far fa-heart" v-else></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home"
                    type="button" role="tab" aria-controls="pills-home"
                    aria-selected="true">@lang("Описание")</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile"
                    type="button" role="tab" aria-controls="pills-profile"
                    aria-selected="false">@lang("Отзывы")</button>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <div class="description">
                {!! $translate->content !!}
            </div>
        </div>
        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">...</div>
    </div>
@endsection

@push('head')
    <link rel="stylesheet" href="/assets/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
    <style>
        .slick-list {
            padding-bottom: 20px;
        }

        .slick-prev:before, .slick-next:before {
            color: #333333;
        }

        .slick-slide {
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
@endpush

@push('scripts')
    <script src="/assets/slick/slick.min.js"></script>
    <script type="application/javascript">
        $(function () {
            $('.photos-slick').slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
            // $('.photos-slick .gallery-image').on('mouseover', function () {
            //     console.log('hovered');
            //     $('.main-photo').attr('src', $(this).attr('src'));
            // });
        });
    </script>
@endpush
