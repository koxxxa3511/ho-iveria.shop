<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RubricRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|min:2|max:255|string',
            'image' => 'required_if:id,null|image'
        ];
    }

    public function authorize(): bool
    {
        return \Auth::check();
    }
}
