<?php

namespace App\Observers;

use App\Models\ProductCategory;
use App\Models\Translate;
use Illuminate\Support\Facades\Auth;

class ProductCategoryObserver
{
    public function updating(ProductCategory $productCategory)
    {
        if ($productCategory->isDirty('image_path') && !empty($productCategory->image_path)) {
            \Storage::delete($productCategory->getOriginal('image_path'));
        }
    }

    public function deleting(ProductCategory $productCategory)
    {
        \Storage::deleteDirectory('/categories/' . $productCategory->id);
    }
}
