@extends('layouts.admin')
@section('content')
    @php
        /**
        * @mixin \App\Models\Rubric $rubrics
        *
        */
    @endphp
    <h2>Рубрики блога</h2>
    <div class="text-end my-2">
        <a href="{{ route('admin-rubric.create') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-plus"></i>
            Добавить рубрику
        </a>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-striped table-sm">
            <thead>
            <tr>
                <th>Картинка</th>
                <th>Название</th>
                <th>*</th>
            </tr>
            </thead>
            <tbody>
            @foreach($rubrics as $rubric)
                <tr>
                    <td>
                        @if($rubric->image_path)
                            <a href="{{ $rubric->image }}" data-fancybox>
                                <img src="{{ $rubric->image }}" alt="" class="img-fluid"
                                     width="50">
                            </a>
                        @else
                            -
                        @endif
                    </td>
                    <td>{{ $rubric->translate->title ?? '<TRANSLATE NOT FOUND>' }}</td>
                    <td>
                        @if($rubric->id != $rubric::WITHOUT_RUBRIC)
                            <a href="{{ route('admin-rubric.edit', $rubric) }}" class="link-warning">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="{{ route('admin-rubric.delete', $rubric) }}" class="link-danger"
                               data-confirm="Уверенны ?">
                                <i class="fa fa-trash"></i>
                            </a>
                        @else
                            <strong class="text-danger">Denied</strong>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
