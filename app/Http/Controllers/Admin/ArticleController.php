<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ArticleSaveRequest;
use App\Models\Article;
use App\Repositories\ArticleRepository;
use App\Repositories\RubricRepository;
use App\Services\ArticleService;
use App\Services\TranslateService;

class ArticleController extends Controller
{
    /**
     * Using to get data from articles table
     *
     * @var ArticleRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $articleRepository;

    /**
     * Using to save data in articles table
     *
     * @var ArticleService|\Illuminate\Contracts\Foundation\Application|mixed ]
     */
    private $articleService;

    /**
     * Using to get data from rubrics table
     *
     * @var RubricRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $rubricRepository;

    /**
     * Using for save data in translates table
     *
     * @var TranslateService|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $translateService;

    public function __construct()
    {
        $this->articleRepository = app(ArticleRepository::class);
        $this->articleService = app(ArticleService::class);
        $this->rubricRepository = app(RubricRepository::class);
        $this->translateService = app(TranslateService::class);
    }
    public function index()
    {
        $articles = $this->articleRepository->getAllWithPaginate(20, 'id');

        return view('admin.blog.articles.index', compact('articles'));
    }
    public function create()
    {
        $article = new Article();
        $rubrics = $this->rubricRepository->getForSelect();

        return view('admin.blog.articles.form', compact('article', 'rubrics'));
    }
    public function edit($id)
    {
        $article = $this->articleRepository->getEdit($id);
        if (empty($article)) {
            return abort(404);
        }
        $rubrics = $this->rubricRepository->getForSelect();

        return view('admin.blog.articles.form', compact('article', 'rubrics'));
    }
    public function save(ArticleSaveRequest $request)
    {
        $article = $this->articleService->handle($request);
        $this->translateService->handle($article, $request);

        return flash('Сохранено');
    }
    public function delete(Article $article)
    {
        Article::destroy($article->id);

        return flash('Удалено');
    }
}
