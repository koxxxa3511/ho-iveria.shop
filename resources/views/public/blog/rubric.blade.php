@extends('layouts.default')
@section("breadcrumbs")
    <div class="active">
        <a href="#">@lang("Блог")</a>
    </div>
@endsection
@section("breadcrumbs")
    <div>
        <a href="{{ route('public.rubric-index') }}">
            @lang("Блог")
        </a>
    </div>
    <div class="active">
        <a href="#">{{ $translate->title }}</a>
    </div>
@endsection
@section("content")
    @include('includes.breadcrumbs')
    <h4 class="section-title">{{ $translate->title }}</h4>
    <div class="row">
        @foreach($articles as $article)
        <div class="col-lg-3 mt-3">
            <a href="{{ route('public-article', ['rubricSlug' => $translate->slug, 'articleSlug' => $article->translate->slug]) }}" class="article">
                <img src="{{ $article->image }}" alt="" class="img-fluid">
                <div class="article-body">
                    <div class="title">{{ $article->translate->title }}</div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
@endsection
