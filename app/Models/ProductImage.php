<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $appends = [
        'full_path'
    ];

    public function getFullPathAttribute(){
        return '/storage/' . $this->path;
    }

    /**
     * Image saver for Service
     *
     * @param $filePath
     */

    public function setImage($filePath){
        $this->path = $filePath;
        $this->save();
    }
}
