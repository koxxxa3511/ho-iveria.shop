<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategorySaveRequest;
use App\Models\ProductCategory;
use App\Models\Translate;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\TranslateRepository;
use App\Services\ProductCategoryService;
use App\Services\TranslateService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Using to get data from product_categories table
     *
     * @var ProductCategoryRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $productCategoryRepository;

    /**
     * Using to save data in product_categories table
     *
     * @var ProductCategoryService|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $productCategoryService;

    /**
     * Using to get data from translates table
     *
     * @var TranslateRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $translateRepository;

    /**
     * Using for save data in translates table
     *
     * @var TranslateService|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $translateService;

    public function __construct()
    {
        $this->productCategoryRepository = app(ProductCategoryRepository::class);
        $this->productCategoryService = app(ProductCategoryService::class);
        $this->translateRepository = app(TranslateRepository::class);
        $this->translateService = app(TranslateService::class);
    }

    public function index()
    {
        $categories = $this->productCategoryRepository->getAll();

        return view('admin.categories.index', compact('categories'));
    }

    public function create()
    {
        $category = new ProductCategory();

        $translates = ['ru' => '', 'ua' => ''];

        return view('admin.categories.form', compact('category', 'translates'));
    }

    public function edit(ProductCategory $category)
    {
        $translates = $this->translateRepository
            ->getAllForModel(ProductCategory::class, $category->id)
            ->pluck('title', 'locale');

        return view('admin.categories.form', compact('category', 'translates'));
    }

    public function save(CategorySaveRequest $request)
    {
        $category = $this->productCategoryService->handle($request);

        foreach ($request->input('titles') as $locale => $title) {
            $array = compact('locale', 'title');
            $array = new Request($array);

            $this->translateService->handle($category, $array);
        }

        flash('Сохранено');

        return to_route('admin-category.index');
    }

    public function delete(ProductCategory $category)
    {
        ProductCategory::destroy($category->id);

        return flash('Удалено');
    }
}
