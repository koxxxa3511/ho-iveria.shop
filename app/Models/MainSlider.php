<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MainSlider extends Model
{
    const TYPE_DESKTOP = 1;
    const TYPE_MOBILE = 2;

    use HasFactory;

    protected $fillable = ['id', 'type_id'];

    public $timestamps = false;

    public function getFullPathAttribute(){
        return '/storage/' . $this->path;
    }

    /**
     * Image saver for Service
     * @param $image
     */

    public function setImage($image){
        $this->path = $image;
        $this->save();
    }
}
