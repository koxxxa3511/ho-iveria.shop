<?php

namespace App\View\Components;

use App\Models\MainSlider;
use Illuminate\View\Component;

class MainCarousel extends Component
{
    public $images;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->images = MainSlider::all()->groupBy('type_id');
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.main-carousel');
    }
}
