<?php

namespace App\Repositories;

use App\Models\Product as Model;

class ProductRepository extends BaseRepository
{

    public function __construct() {
        parent::__construct(new Model());
    }

    /**
     * Get the model for edit in the admin panel
     *
     * @param $id
     * @return mixed
     */

    public function getAll($relations = ['translate'])
    {
        return parent::getAll($relations);
    }

    /**
     * Get the model for edit in the admin panel
     *
     * @param $id
     * @return mixed
     */

    public function getEdit($id, $relations = ['translate'])
    {
        return $this->startConditions()->with($relations)->find($id);
    }

    /**
     * Get the model for the front-end side
     *
     * @param $id
     * @return mixed
     */

    public function getById($id, $relations = ['translate'])
    {
        return parent::getById($id, $relations);
    }

    /**
     * Get only these records that have translates
     *
     * @param $id
     * @return mixed
     */

    public function getAllWithTranslates($relations = ['translate'])
    {
        $result = $this->startConditions()->with($relations)->get();
        $result = $result->filter(function ($product) {
            $hasTranslate = (bool) $product->translate ?? false;
            $catHasTranslate = (bool) ($product->category->translate ?? false);
            $hasMainImage = (bool) $product->main_image ?? false;
            $access = ($hasTranslate && $catHasTranslate && $hasMainImage);

            return $access;
        });

        return $result;
    }


    /**
     * Get a list of rubrics to show select list.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */

    public function getForSelect(array $relations = ['main_image', 'translate', 'category.translate'])
    {
        return $this->startConditions()->with($relations)->get();
    }

}
