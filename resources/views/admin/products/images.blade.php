@extends('layouts.admin')
@section('content')
    <h2>Фотографии для {{ $product->translate->title ?? ''}}</h2>
    <div class="alert alert-danger mt-3" role="alert">
        Первая картинка - будет основной картинкой товара.
    </div>
    <div class="my-3">
        <form action="{{ route('admin-product.image-upload') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="product_id" value="{{ $product->id }}">
            <label for="file">Новая картинка</label>
            <input type="file" name="file" id="file" required accept="image/jpeg, image/png, image/webp">
            <div class="text-end mt-3">
                <button type="submit" class="btn btn-sm btn-primary">
                    <i class="fa fa-upload"></i>
                    Загрузить
                </button>
            </div>
        </form>
    </div>
    <div class="mt-3 table-responsive">
        <table class="table table-striped table-sm table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Картинка</th>
                <th>*</th>
            </tr>
            </thead>
            <tbody>
            @foreach($images as $image)
                <tr>
                    <td>{{ $image->id }}</td>
                    <td>
                        <a href="{{ $image->full_path }}" data-fancybox>
                            <img src="{{ $image->full_path }}" alt="" class="img-fluid" width="50">
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('admin-product.image-delete', ['productImage' => $image]) }}"
                           class="btn btn-danger btn-sm" data-confirm="Уверенны?">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
