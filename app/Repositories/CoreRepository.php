<?php

namespace App\Repositories;


use App\Models\Article;
use Illuminate\Database\Eloquent\Model;

abstract class CoreRepository{
    /**
     *
     * @var Model
     */
    protected $model;

    /**
     * CoreRepository constructor.
     */
    public function __construct()
    {
        $this->model =$this->getModelClass();
    }

    /**
     * @return mixed
     */
    abstract protected function getModelClass();

    /**
     * @return \Illuminate\Contracts\Foundation\Application|Model|mixed
     */
    protected function startConditions(){
        return new $this->model;
    }
}
