<ul class="leftside-menu">
    <li>
        <a href="{{ route('admin-users') }}">
            <i class="fa fa-users"></i>
            Пользователи
        </a>
    </li>
    <li>
        <a href="{{ route('admin-main-slider.index') }}">
            <i class="fa fa-images"></i>
            Картинки главного слайдера
        </a>
    </li>
    <li>
        <a href="{{ route('admin-category.index') }}">
            <i class="fa fa-list-ul"></i>
            Категории товаров
        </a>
    </li>
    <li>
        <a href="{{ route('admin-product.index') }}">
            <i class="fa fa-boxes"></i>
            Товары
        </a>
    </li>
    <li>
        <a href="{{ route('admin-rubric.index') }}">
            <i class="fa fa-list-ul"></i>
            Рубрики блога
        </a>
    </li>
    <li>
        <a href="{{ route('admin-article.index') }}">
            <i class="fa fa-newspaper"></i>
            Статьи
        </a>
    </li>
    <li>
        <a href="{{ route('admin-settings') }}">
            <i class="fa fa-cogs"></i>
            Настройки
        </a>
    </li>
</ul>
