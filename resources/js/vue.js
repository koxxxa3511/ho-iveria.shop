import Vuex from 'vuex';
import storeData from './store';

window.Vue = require('vue/dist/vue.min');

Vue.use(Vuex);

const store = new Vuex.Store(storeData);

let Header = new Vue({
    el: '#header',
    store,
    data: {
        search: '',
    },
    mounted() {
        this.$store.dispatch("getData");
    },
    computed: {
        sortedProducts: function () {
            return this.$store.state.products.filter((product) => {
                let hasInContent = product.translate.content.toLowerCase().indexOf(this.search.toLowerCase()) > -1,
                    hasInTitle = product.translate.title.toLowerCase().indexOf(this.search.toLowerCase()) > -1;

                return hasInContent || hasInTitle;
            });
        },
        cart_count() {
            return _.size(this.$store.state.cart) - 1;
        }
    },
    watch: {
        cart_count() {
            return _.size(this.$store.state.cart) - 1;
        }
    },
    methods: {
        removeItem(product_id) {
            // this.cart.splice(product_id, 1);
            this.$store.dispatch('removeFromCart', product_id)
        },
        clearCart() {
            this.$store.dispatch('clearCart');
        },
        changeCount(product_id, count) {
            if (count > 0) {
                this.$store.dispatch('changeCount', {'product_id': product_id, 'count': count});
            }
        },
        addToCart(item_id) {
            this.$store.dispatch('addToCart', item_id);
        }
    }
});
window.Header = Header;

let IndexPage = new Vue({
    el: '#index',
    store,
    computed:{
        favorites() {
            return this.$store.state.favorites;
        }
    },
    methods: {
        addToFavorite: function (item_id) {
            this.$store.dispatch('addToFavorite', item_id);
        },
        addToCart: function (item_id) {
            this.$store.dispatch('addToCart', item_id);
        }
    }
});

let FavoritePage = new Vue({
    el: '#favorite',
    store,
    computed: {
        favorites() {
            return this.$store.state.favorites;
        }
    },
    methods: {
        addToFavorite: function (item_id) {
            this.$store.dispatch('addToFavorite', item_id);
        },
    }
});

let Product = new Vue({
    el: '#public-product',
    store,
    data: {
        count: 1
    },
    computed: {
        favorites() {
            return this.$store.state.favorites;
        }
    },
    methods: {
        addToCart(item_id) {
            if (this.count >= 1) {
                this.$store.dispatch('addToCart', {'item_id': item_id, 'count': this.count});
            }
        },
        subItem() {
            if (this.count > 1) {
                this.count--;
            }
        },
        addToFavorite: function (item_id) {
            this.$store.dispatch('addToFavorite', item_id);
        },
    }
})
