<a href="{{ route('public-product', ['category' => $product->categorySlug, 'slug' => $product->transSlug]) }}"
   class="card product product-{{ $product->id }}">
    <div class="add-to-favorite" @click.prevent="addToFavorite({{ $product->id }})">
        <i class="fa fa-heart" v-if="_.includes(this.favorites, {{ $product->id }})"></i>
        <i class="far fa-heart" v-else></i>
    </div>
    @if($product->sale_price)
        <img src="/images/sale.png" alt="" class="img-fluid sale" loading="lazy">
    @endif
    @if($product->main_image_path)
        <img src="{{ $product->mainImagePath }}" alt="" class="img-fluid card-img-top" loading="lazy">
    @endif
    <div class="card-body{{ $product->sale_price ? ' with-sale' : '' }}">
        <div class="card-title text-center">{{ $product->transTitle  ?? '' }}</div>
        @if($product->sale_price)
            <div class="price old-price"><span>{{ number_format($product->price, 0, '.', ' ') }}</span> грн</div>
            <div class="price"><span>{{ number_format($product->sale_price, 0, '.', ' ') }}</span> грн</div>
        @else
            <div class="price"><span>{{ number_format($product->price, 0, '.', ' ') }}</span> грн</div>
        @endif
        <button class="add-to-cart" @click.prevent="addToCart({{ $product->id }})">
            <i class="fa fa-shopping-cart"></i>
        </button>
    </div>
</a>
