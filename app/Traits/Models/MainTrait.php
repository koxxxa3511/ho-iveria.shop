<?php

namespace App\Traits\Models;

use App\Models\Translate;

trait MainTrait
{
    public static function bootMainTrait()
    {
        static::saving(function ($model) {
            if (!$model->exists) {
                $model->user_id = \Auth::id();
            }
        });

        static::deleting(function ($model) {
            Translate::where([
                                 'translateable_id'   => $model->id,
                                 'translateable_type' => get_class($model)
                             ])
                     ->delete();
        });
    }
}
