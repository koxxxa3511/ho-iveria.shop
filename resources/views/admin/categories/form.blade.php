@extends('layouts.admin')
@section('content')
    <h2>
        @if($category->exists)
            Редактирование {{ $category->translate->title ?? '' }}
        @else
            Добавление категории
        @endif
    </h2>
    <form action="{{ route('admin-category.save') }}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{ $category->id }}">
        <div class="mb-3 row">
            <div class="col-lg">
                <label for="ru_title">Название (RU)</label>
                <input type="text" name="titles[ru]" id="ru_title" class="form-control" required
                       value="{{ old('titles[ru]', $translates['ru'] ?? '') }}">
            </div>
            <div class="col-lg">
                <label for="ua_title">Название (UA)</label>
                <input type="text" name="titles[ua]" id="ua_title" class="form-control" required
                       value="{{ old('titles[ua]', $translates['ua'] ?? '') }}">
            </div>
            <div class="col-lg">
                <label for="file">Картинка</label>
                <input type="file" name="file" id="file" class="form-control"
                       accept="image/jpeg, image/png" {{ $category->exists ? '' : 'required' }}>
                @if($category->image_path)
                    <div class="text-center mt-3">
                        <img src="{{ $category->image }}" alt="" class="img-fluid" width="200">
                    </div>
                @endif
            </div>
        </div>
        <div class="text-end mt-3">
            <button type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-save"></i>
                Сохранить
            </button>
        </div>
    </form>
@endsection

@push('scripts')
    <!-- include summernote css/js -->
    <link href="/assets/summernote/summernote-lite.min.css" rel="stylesheet">
    <script src="/assets/summernote/summernote-lite.min.js"></script>
    <script>
        $(function () {
            $('#summernote').summernote({
                height: 300,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ol', 'ul', 'paragraph', 'height']],
                    ['table', ['table']],
                    ['insert', ['link']],
                    ['view', ['undo', 'redo', 'fullscreen', 'codeview', 'help']]
                ]
            });
        })
    </script>
@endpush
