@if($product->images()->count() > 1)
    <div class="photos-slick mt-3">
        @foreach($product->images as $image)
            <div>
                <a href="{{ $image->full_path }}" data-fancybox="gallery">
                    <img src="{{ $image->full_path }}" alt="" class="img-fluid gallery-image">
                </a>
            </div>
        @endforeach
    </div>
@endif
