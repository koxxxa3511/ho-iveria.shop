@extends('layouts.default')
@section('breadcrumbs')
    <div>
        <a href="#" class="active">@lang("Мои товары")</a>
    </div>
@endsection
@section('content')
    <div class="box">
        <h4 class="section-title">@lang("Мои товары")</h4>
        <div class="row">
            @foreach($products as $product)
                <div class="col-lg-3">
                    <x-product :product="$product"></x-product>
                </div>
            @endforeach
        </div>
    </div>
@endsection
