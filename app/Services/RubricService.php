<?php

namespace App\Services;

use App\Models\Rubric;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class RubricService extends BaseService
{
    public function __construct(Rubric $rubric) {
        $this->model = $rubric;
    }

    public function handle(Request $request){
        $baseInputs = ['id' => $request->input('id')];

         return parent::firstOrCreate($baseInputs, $request->file('image'));
    }
}
