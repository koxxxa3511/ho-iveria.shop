<?php

namespace App\Observers;

use App\Models\MainSlider;

class MainSliderObserver
{
    public function updated(MainSlider $mainSlider)
    {
        if ($mainSlider->isDirty('path') && !empty($mainSlider->path)) {
            \Storage::delete($mainSlider->getOriginal('path'));
        }
    }

    public function deleting(MainSlider $mainSlider)
    {
        \Storage::delete($mainSlider->path);
    }
}
