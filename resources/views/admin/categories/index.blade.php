@extends('layouts.admin')
@section('content')
    <h2>Категории товаров</h2>
    <div class="text-end my-2">
        <a href="{{ route('admin-category.create') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-plus"></i>
            Добавить категорию
        </a>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-striped table-sm">
            <thead>
            <tr>
                <th>Картинка</th>
                <th>Название</th>
                <th>*</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>
                            <a href="{{ $category->image }}" data-fancybox>
                                <img src="{{ $category->image }}" alt="" class="img-fluid"
                                     width="50">
                            </a>
                    </td>
                    <td>{{ $category->translate->title ?? '' }}</td>
                    <td>
                        <a href="{{ route('admin-category.edit', $category) }}" class="link-warning">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="{{ route('admin-category.delete', $category) }}" class="link-danger"
                           data-confirm="Уверенны ?">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
