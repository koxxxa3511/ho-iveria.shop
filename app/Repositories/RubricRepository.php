<?php

namespace App\Repositories;

use App\Models\Rubric as Model;

class RubricRepository extends BaseRepository
{
    public function __construct() {
        parent::__construct(new Model());
    }

    /**
     * Get all rubrics
     *
     * @param string[] $relations
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */

    public function getAll($relations = ['translate']){
        return parent::getAll($relations);
    }

    /**
     * Get the model for edit in the admin panel
     *
     * @param $id
     * @return mixed
     */


    public function getForEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    /**
     * Get the model for front end side
     *
     * @param $id
     * @return mixed
     */


    public function getById($id, $relations = ['translate'])
    {
        return parent::getById($id, $relations);
    }

    /**
     * Get a list of rubrics to show select list.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */

    public function getForSelect()
    {
        $columns = ['id', 'image_path'];

        $result = $this->startConditions()
            ->with('translate:id,title,translateable_type,translateable_id')
            ->select($columns)
            ->get();

        return $result;
    }
}
