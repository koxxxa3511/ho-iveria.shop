<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductImageUploadRequest;
use App\Http\Requests\Admin\ProductRequest;
use App\Models\Product;
use App\Models\ProductImage;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use App\Services\ImageUploadService;
use App\Services\ProductService;
use App\Services\TranslateService;
use Buglinjo\LaravelWebp\Webp;

class ProductController extends Controller
{
    /**
     * Using to get data from products table
     *
     * @var ProductRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $productRepository;

    /**
     * Using to save data in products table
     *
     * @var ProductService|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $productService;

    /**
     * @var ProductCategoryRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $productCategoryRepository;

    /**
     * Using for save data in translates table
     *
     * @var TranslateService|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $translateService;

    /**
     * Service to save image
     *
     * @var ImageUploadService|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $imageUploadService;

    public function __construct()
    {
        $this->productRepository = app(ProductRepository::class);
        $this->productService = app(ProductService::class);
        $this->productCategoryRepository = app(ProductCategoryRepository::class);
        $this->translateService = app(TranslateService::class);
        $this->imageUploadService = app(ImageUploadService::class);
    }

    public function index()
    {
        $products = $this->productRepository->getAll(['main_image', 'translate', 'category.translate']);
        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        $product = new Product();
        $product_categories = $this->productCategoryRepository->getForSelect();

        return view('admin.products.form', compact('product', 'product_categories'));
    }

    public function edit(Product $product)
    {
        $product_categories = $this->productCategoryRepository->getForSelect();

        return view('admin.products.form', compact('product', 'product_categories'));
    }

    public function save(ProductRequest $request)
    {
        $product = $this->productService->handle($request);
        $this->translateService->handle($product, $request);

        flash('Сохранено');
        return to_route('admin-product.index');
    }

    public function delete(Product $product)
    {
        Product::destroy($product->id);

        return flash('Удалено');
    }

    public function images($id)
    {
        $product = $this->productRepository->getById($id);
        $images = $product->images()
                          ->oldest('id')
                          ->get();
        return view('admin.products.images', compact('product', 'images'));
    }

    public function imageUpload(ProductImageUploadRequest $request)
    {
        $product = $this->productRepository->getById($request->product_id);
        $pi = new ProductImage();
        $pi->product_id = $product->id;
        $filePath = "products/product-$product->id/images";
        $pi->path = $request->file('file')->store($filePath);

        $this->imageUploadService->uploadInWebP($pi, $request->file( 'file'), ['file_path' => $filePath]);

        return flash('Сохранено');
    }

    public function imageDelete(ProductImage $productImage)
    {
        ProductImage::destroy($productImage->id);

        return flash('Удалено');
    }
}
