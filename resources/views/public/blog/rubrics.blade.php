@extends('layouts.default')
@section("content")
    <h4 class="section-title">@lang("Блог")</h4>
    @foreach($rubrics->chunk(4) as $chunk)
        <div class="row">
            @foreach($chunk as $rubric)
                <div class="col-lg-3">
                    <a href="{{ route('public.rubric', $rubric->translate->slug) }}" class="rubric">
                        <img src="{{ $rubric->image }}" alt="" class="img-fluid">
                        <div class="rubric-body">
                            <div class="title">{{ $rubric->translate->title }}</div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    @endforeach
@endsection
