<?php

namespace App\Services;

use App\Models\Translate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class TranslateService extends BaseService
{

    public function __construct(Translate $translate)
    {
        $this->model = $translate;
    }

    public function handle(Model $model, Request $request)
    {
        $baseInputs = [
            'locale'             => $request->input('locale') ?? getLocale(),
            'translateable_id'   => $model->id,
            'translateable_type' => get_class($model)
        ];
        $changeInputs = [
            'title'   => $request->input('title'),
            'content' => $request->input('content')
        ];
        return parent::updateOrCreate($baseInputs, $changeInputs);
    }
}
