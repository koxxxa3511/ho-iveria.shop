<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Rubric;
use App\Models\Translate;
use App\Repositories\ArticleRepository;
use App\Repositories\RubricRepository;
use App\Repositories\TranslateRepository;

class BlogController extends Controller
{
    /**
     * @var RubricRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $rubricRepository;
    /**
     * @var TranslateRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $translateRepository;
    /**
     * @var ArticleRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $articleRepository;

    public function __construct() {
        $this->rubricRepository = app(RubricRepository::class);
        $this->articleRepository = app(ArticleRepository::class);
        $this->translateRepository = app(TranslateRepository::class);
    }

    public function index()
    {
        $rubrics = $this->rubricRepository->getAll();
        $rubrics = $rubrics->where('id', '!=', Rubric::WITHOUT_RUBRIC);
        return view('public.blog.rubrics', compact('rubrics'));
    }

    public function rubric($slug)
    {
        $translate = $this->translateRepository->getBySlugName($slug, Rubric::class);
        if(empty($translate)){
            abort(404);
        }
        $articles = $this->articleRepository->getAllByRubricId($translate->translateable_id);

        return view('public.blog.rubric', compact('articles', 'translate'));
    }

    public function article($rubricSlug, $articleSlug)
    {
        $translateRubric = $this->translateRepository->getBySlugName($rubricSlug, Rubric::class);
        if(empty($translateRubric)){
            abort(404);
        }
        $rubric = $this->rubricRepository->getById($translateRubric->translateable_id);

        $translateArticle = $this->translateRepository->getBySlugName($articleSlug, Article::class);
        if(empty($translateRubric)){
            abort(404);
        }
        $article = $this->articleRepository->getById($translateArticle->translateable_id);

        return view('public.blog.article', compact('translateArticle', 'article', 'translateRubric', 'rubric'));
    }
}
