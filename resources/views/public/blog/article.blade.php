@extends('layouts.default')
@section("breadcrumbs")
    <div>
        <a href="{{ route('public.rubric-index') }}">
            @lang("Блог")
        </a>
    </div>
    <div class="active">
        <a href="{{ route('public.rubric', $translateRubric->slug) }}">{{ $translateRubric->title }}</a>
    </div>
@endsection
@section('content')
    @include('includes.breadcrumbs')
    <div class="box">
        {!! $translateArticle->content !!}
    </div>
@endsection
