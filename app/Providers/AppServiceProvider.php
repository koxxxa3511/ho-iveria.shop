<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\Rubric;
use App\Models\Translate;
use App\Observers\ArticleObserver;
use App\Observers\ProductCategoryObserver;
use App\Observers\ProductImageObserver;
use App\Observers\ProductObserver;
use App\Observers\RubricObserver;
use App\Observers\TranslateObserver;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        Blade::if('role', function ($role_id_or_name) {
            $user = auth()->user();
            return $user && $user->hasRole($role_id_or_name);
        });


        Article::observe(ArticleObserver::class);
        Translate::observe(TranslateObserver::class);
        Rubric::observe(RubricObserver::class);
        Product::observe(ProductObserver::class);
        ProductImage::observe(ProductImageObserver::class);
        ProductCategory::observe(ProductCategoryObserver::class);
    }
}
