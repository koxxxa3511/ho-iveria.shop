<?php

namespace App\Models;

use App\Traits\Models\MainTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use HasFactory, MainTrait;
    protected $fillable = ['id'];

    public $timestamps = false;

    public function getImageAttribute(){
        return '/storage/app/public/' . $this->image_path;
    }

    public function translate(){
        return $this->morphOne(Translate::class, 'translateable')->whereLocale(getLocale());
    }

    public function getTransTitleAttribute(){
        return $this->translate->title ?? '';
    }

    /**
     * Image saver for Service
     * @param $image
     */

    public function setImage($image){
        $this->image_path = $image;
        $this->save();
    }
}
