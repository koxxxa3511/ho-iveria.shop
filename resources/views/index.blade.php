@extends('layouts.public')
<?php
/**
 * @mixin \App\Models\Product
 */
?>
@section('content')
    {{--    <x-main-carousel></x-main-carousel>--}}
    @if($novelties->count()) <!-- todo: Need to test... -->
    <h4 class="section-title">@lang("Новинки")</h4>
    <div class="slick-slider mt-3">
        @foreach($novelties as $product)
            <div>
                <x-product :product="$product"></x-product>
            </div>
        @endforeach
    </div>
    @endif
    @if($tops->count()) <!-- todo: Need to test... -->
    <h4 class="section-title">@lang("Топ товары")</h4>
    <div class="slick-slider mt-3">
        @foreach($tops as $product)
            <div>
                <x-product :product="$product"></x-product>
            </div>
        @endforeach
    </div>
    @endif
    @if($sales->count()) <!-- todo: Need to test... -->
    <h4 class="section-title">@lang("Акционные товары")</h4>
    <div class="slick-slider mt-3">
        @foreach($sales as $product)
            <div>
                <x-product :product="$product"></x-product>
            </div>
        @endforeach
    </div>
    @endif
    @if($products->count()) <!-- todo: Need to test... -->
    <h4 class="section-title">@lang("Все товары")</h4>
    @foreach($products->chunk(4) as $chunk)
        <div class="row mt-3">
            @foreach($chunk as $product)
                <div class="col-lg-3">
                    <x-product :product="$product"></x-product>
                </div>
            @endforeach
        </div>
    @endforeach
    @endif
@endsection

@push('head')
    <link rel="stylesheet" href="/assets/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
    <style>
        .slick-list {
            padding-bottom: 20px;
        }

        .slick-prev:before, .slick-next:before {
            color: #333333;
        }
    </style>
@endpush

@push('scripts')
    <script src="/assets/slick/slick.min.js"></script>
    <script>
        $(function () {
            $('.slick-slider').slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        });
    </script>
@endpush
