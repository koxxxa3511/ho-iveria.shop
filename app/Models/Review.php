<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*
 * Comments will be available only for the site, products and comments on comments.
 */

class Review extends Model
{
    const TYPE_PRODUCT = 1;
    const TYPE_SITE = 2;
    const TYPE_COMMENT_ON_COMMENT = 3;

    use HasFactory;
}
