<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\UserSaveRequest;
use App\Models\Role;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * @var UserRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $userRepository;

    public function __construct()
    {
        $this->userRepository = app(UserRepository::class);
    }

    public function index(){
    	return redirect()->route('admin-users');
    }
    public function users(){
    	$view = view('admin.users.index');
    	$view->users = $this->userRepository->getAllWithPaginate(20, 'created_at', ['role']);
    	return $view;
    }
    public function create(){
    	$user = new User();
    	$user->role_id = Role::ID_USER;

	    $view = view('admin.users.form');
	    $view->user = $user;
	    $view->roles = Role::orderBy('id')->get();
	    return $view;
    }
    public function edit($id){
	    $view = view('admin.users.form');
	    $view->user = $this->userRepository->getEdit($id);
	    $view->roles = Role::orderBy('id')->get();
	    return $view;
    }
    public function delete($id){
	    $user = $this->userRepository->getEdit($id);;
	    $user->delete();

	    flash('User deleted');
	    return back();
    }
    public function save(UserSaveRequest $request){
		if($request->id){
			$user = $this->userRepository->getEdit($request->id);
			if($request->password){
				$user->password = \Hash::make($request->password);
			}
		}else{
			$user = new User();
			$user->password = \Hash::make($request->password);
		}
		$user->fill($request->except('password'));
		$user->role_id = $request->role_id;
		$user->save();

		flash('User saved');

		return back();
    }

    public function favoriteUser(User $user){
        $products = $user->favorites;

        return view('admin.users.favorites', compact('products', 'user'));
    }
}
