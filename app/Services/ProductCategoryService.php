<?php

namespace App\Services;

use App\Models\ProductCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ProductCategoryService extends BaseService
{
    public function __construct(ProductCategory $category) {
        $this->model = $category;
    }

    public function handle(Request $request){
        $baseInputs = [
            'id' => $request-> input('id')
        ];
        return parent::firstOrCreate($baseInputs, $request->file('file'));
    }
}
