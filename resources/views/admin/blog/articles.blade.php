@extends('layouts.admin')
@section("content")
    <h2>Статьи</h2>
    <div class="text-end">
        <a href="#" class="btn btn-primary btn-sm">
            <i class="fa fa-plus"></i>
            Добавить статью
        </a>
    </div>
    <div class="table-response mt-3">
        <table class="table table-hover table-striped table-sm">
            <thead>
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>Рубрика</th>
                <th>Создана</th>
                <th>*</th>
            </tr>
            </thead>
            <tbody>
            @foreach($articles as $article)
                <tr>
                    <td>{{ $article->id }}</td>
                    <td>{{ $article->translate->title }}</td>
                    <td>{{ $article->rubric->translate->title }}</td>
                    <td>{{ $article->postedDate }}</td>
                    <td>
                        <a href="#" class="link-warning">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="#" class="link-danger" data-confirm="Уверенны?">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
