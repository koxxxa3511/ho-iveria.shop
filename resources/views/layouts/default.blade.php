<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    @auth
        @if(in_array(Auth::user()->role_id, [\App\Models\Role::ID_ADMIN, \App\Models\Role::ID_MODERATOR]))
            <link rel="stylesheet" href="{{ asset('/css/admin.css') }}">
        @endif
    @endauth

    <title>{{ config('app.name') }}</title>

    @stack('head')
</head>
<body>
<div>
    @include('layouts.menu')
    <div class="content" id="{{ Route::current()->getName() }}">
        <div class="container">
            @yield('content')
        </div>
    </div>
    <x-footer></x-footer>
</div>

<div id="ajax-modal-container"></div>
<link rel="stylesheet" data-href="{{ mix('/css/public.css') }}">
<script src="{{ mix('/js/public.js') }}"></script>
<script src="{{ mix('/js/vue.js') }}" async></script>
<script>
    $(function () {
        $('[data-href]').each(function (index, item) {
            $(item).attr('href', $(item).data('href')).removeAttr('data-href');
        });
    });
</script>
@stack('scripts')

@include('includes.flash')
</body>
</html>
