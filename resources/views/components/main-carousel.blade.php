<?php
/**
 * @mixin \App\Models\MainSlider
 */
?>

@if(isset($images[\App\Models\MainSlider::TYPE_DESKTOP]))
    <div id="main_desktop_slider" class="carousel slide d-none d-lg-block mt-3" data-bs-ride="carousel">
        {{--    <div class="carousel-indicators">--}}
        {{--        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"--}}
        {{--                aria-current="true" aria-label="Slide 1"></button>--}}
        {{--        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"--}}
        {{--                aria-label="Slide 2"></button>--}}
        {{--        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"--}}
        {{--                aria-label="Slide 3"></button>--}}
        {{--    </div>--}}
        <div class="carousel-inner">
            @foreach($images[\App\Models\MainSlider::TYPE_DESKTOP] as $image)
                <div class="carousel-item{{ $loop->index == 0 ? ' active' : '' }}">
                    <img src="{{ $image->full_path }}" class="d-block w-100" alt="">
                </div>
            @endforeach
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#main_desktop_slider"
                data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#main_desktop_slider"
                data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
@endif
@if(isset($images[\App\Models\MainSlider::TYPE_MOBILE]))
    <div id="main_mobile_slider" class="carousel slide d-lg-none mt-3" data-bs-ride="carousel">
        {{--    <div class="carousel-indicators">--}}
        {{--        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"--}}
        {{--                aria-current="true" aria-label="Slide 1"></button>--}}
        {{--        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"--}}
        {{--                aria-label="Slide 2"></button>--}}
        {{--        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"--}}
        {{--                aria-label="Slide 3"></button>--}}
        {{--    </div>--}}
        <div class="carousel-inner">
            @foreach($images[\App\Models\MainSlider::TYPE_MOBILE] as $image)
                <div class="carousel-item{{ $loop->index == 0 ? ' active' : '' }}">
                    <img src="{{ $image->full_path }}" class="d-block w-100" alt="">
                </div>
            @endforeach
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#main_mobile_slider"
                data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#main_mobile_slider"
                data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
@endif
