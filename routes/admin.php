<?php

use App\Http\Controllers\Admin\MainSliderController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ProductController;
use \App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\RubricController;

Route::get('/', [UserController::class, 'index'])->name('index');
Route::get('users', [UserController::class, 'users'])->name('users');
Route::get('user/create', [UserController::class, 'create'])->name('user-create');
Route::get('user/{id}/edit', [UserController::class, 'edit'])->name('user-edit');
Route::get('user/{id}/delete', [UserController::class, 'delete'])->name('user-delete');
Route::post('user/save', [UserController::class, 'save'])->name('user-save');
Route::get('user/products/favorites/{user}', [UserController::class, 'favoriteUser'])->name('user-favorite');

Route::get('settings', [SettingController::class, 'index'])->name('settings');
Route::post('settings/save', [SettingController::class, 'save'])->name('settings-save');

Route::get('sliders/main', [MainSliderController::class, 'index'])->name('main-slider.index');
Route::get('sliders/main/edit/{mainSlider}', [MainSliderController::class, 'edit'])->name('main-slider.edit');
Route::get('sliders/main/delete/{mainSlider}', [MainSliderController::class, 'delete'])->name('main-slider.delete');
Route::post('sliders/main/save', [MainSliderController::class, 'save'])->name('main-slider.save');

Route::get('product', [ProductController::class, 'index'])->name('product.index');
Route::get('product/create', [ProductController::class, 'create'])->name('product.create');
Route::get('product/edit/{product}', [ProductController::class, 'edit'])->name('product.edit');
Route::get('product/delete/{product}', [ProductController::class, 'delete'])->name('product.delete');
Route::post('product/save', [ProductController::class, 'save'])->name('product.save');
Route::get('product/images/{product}', [ProductController::class, 'images'])->name('product.images');
Route::post('product/image/upload', [ProductController::class, 'imageUpload'])->name('product.image-upload');
Route::get('product/image/delete/{productImage}', [ProductController::class, 'imageDelete'])->name('product.image-delete');

Route::get('articles', [ArticleController::class, 'index'])->name('article.index');
Route::get('article/create', [ArticleController::class, 'create'])->name('article.create');
Route::get('article/edit/{id}', [ArticleController::class, 'edit'])->name('article.edit');
Route::post('article/save', [ArticleController::class, 'save'])->name('article.save');
Route::get('article/delete/{article}', [ArticleController::class, 'delete'])->name('article.delete');

Route::get('rubrics', [RubricController::class, 'index'])->name('rubric.index');
Route::get('rubric/create', [RubricController::class, 'create'])->name('rubric.create');
Route::get('rubric/edit/{id}', [RubricController::class, 'edit'])->name('rubric.edit');
Route::post('rubric/save', [RubricController::class, 'save'])->name('rubric.save');
Route::get('rubric/delete/{id}', [RubricController::class, 'delete'])->name('rubric.delete');

Route::get('category', [CategoryController::class, 'index'])->name('category.index');
Route::get('category/create', [CategoryController::class, 'create'])->name('category.create');
Route::get('category/edit/{category}', [CategoryController::class, 'edit'])->name('category.edit');
Route::get('category/delete/{category}', [CategoryController::class, 'delete'])->name('category.delete');
Route::post('category/save', [CategoryController::class, 'save'])->name('category.save');
