@extends('layouts.admin')
@section('content')
    <h2>Товары</h2>
    <div class="text-end my-2">
        <a href="{{ route('admin-product.create') }}" class="btn btn-primary btn-sm">
            <i class="fa fa-plus"></i>
            Добавить товар
        </a>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-striped table-sm">
            <thead>
            <tr>
                <th>Картинка</th>
                <th>Название</th>
                <th>Категория</th>
                <th>Цена</th>
                <th>*</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>
                        @if($product->main_image)
                            <a href="{{ $product->main_image->full_path }}" data-fancybox>
                                <img src="{{ $product->main_image->full_path }}" alt="" class="img-fluid"
                                     width="50">
                            </a>
                        @else
                            -
                        @endif
                    </td>
                    <td>{{ $product->translate->title ?? '<TRANSLATE NOT FOUND>' }}</td>
                    <td>{{ $product->category->translate->title ?? '<CATEGORY NOT FOUND>' }}</td>
                    <td>{{ $product->price }} грн</td>
                    <td>
                        <a href="{{ route('admin-product.edit', $product) }}" class="link-warning">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="{{ route('admin-product.images', $product) }}" class="link-info">
                            <i class="fa fa-images"></i>
                        </a>
                        <a href="{{ route('admin-product.delete', $product) }}" class="link-danger"
                           data-confirm="Уверенны ?">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
