<?php

namespace App\Models;

use App\Traits\Models\MainTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory, MainTrait;

    protected $fillable = ['id', 'rubric_id'];

    public function rubric(){
        return $this->belongsTo(Rubric::class);
    }
    public function translate(){
        return $this->morphOne(Translate::class, 'translateable')->whereLocale(getLocale());
    }

    public function getImageAttribute(){
        return '/storage/app/public/' . $this->image_path;
    }

    public function getPostedDateAttribute(){
        return $this->created_at->format('d.m.Y H:i');
    }

    /**
     * Image saver for Service
     * @param $image
     */

    public function setImage($image){
        $this->image_path = $image;
        $this->save();
    }
}
