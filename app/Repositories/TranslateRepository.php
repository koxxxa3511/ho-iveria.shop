<?php

namespace App\Repositories;

use App\Models\Translate as Model;

class TranslateRepository extends BaseRepository
{
    public function __construct() {
        parent::__construct(new Model());
    }

    /**
     * Get the model for edit in the admin panel
     *
     * @param $id
     * @return mixed
     */

    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    /**
     * Get the model for edit in the admin panel by slug name
     *
     * @param $id
     * @return mixed
     */

    public function getBySlugName($slug, $translateable_type)
    {
        return $this->startConditions()
            ->whereLocale(getLocale())
            ->whereTranslateableType($translateable_type)
            ->whereSlug($slug)
            ->first();
    }

    public function getAllForModel($type, $id)
    {
        $result = $this->startConditions()
            ->where(['translateable_type' => $type, 'translateable_id' => $id])
            ->get();

        return $result;
    }

    /**
     * Get a list of rubrics to show select list.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */

    public function getForSelect()
    {
        return $this->startConditions()->with('translate')->get();
    }
}
